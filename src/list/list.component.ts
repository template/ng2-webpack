import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import './list.component.less';
@Component({
  selector: 'ng2w-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit,OnDestroy{
	public count : Number
	public counter$ : Observable<Number>
	private subscription : Subscription 
	constructor(){
	}

	ngOnInit():void{
		this.subscription = Observable.interval(1000)
		.subscribe(number => {
			console.log('***COUNT***',number);
			this.count = number
		});

		this.counter$ = Observable.interval(1000).do(number => console.log('debug COUNTER',number));
	}

	ngOnDestroy():void{
		console.log('Destroy list');
	}

	unsubscribe():void{
		this.subscription.unsubscribe();	
	}

}
